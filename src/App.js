import logo from "./logo.svg";
import "./App.css";
import GlobalStyles from "./styles/GlobalStyles";
import { ThemeProvider } from "styled-components";
import { dark, light } from "./styles/Themes";
import Navigation from "./components/Navigation";
import Roadmap from "./components/sections/Roadmap";
import Showcase from "./components/sections/Showcase";
import Home from "./components/sections/Home";
import About from "./components/sections/About";
import Team from "./components/sections/Team";
import Faq from "./components/sections/Faq";
import Footer from "./components/Footer";
import ScrollToTop from "./components/ScrollToTop";

function App() {
  return (
    <div>
      <GlobalStyles />
      <ThemeProvider theme={light}>
        <Navigation />
        <Home />
        <About />
        <Roadmap />
        <Showcase />
        <Team />
        <Faq />
        <Footer />
        <ScrollToTop />
      </ThemeProvider>
    </div>
  );
}

export default App;
